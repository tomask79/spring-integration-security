## Securing Spring Rest applications backed by Spring Integration via Spring Security ##

To tell you the truth, even if I'm a big fan of Spring applications based on the Spring Integration project, somehow I missed that Spring Integration works well with Spring Security. In terms of that **if you've got authorized user via Spring Security you can use obtained user's role to authorize user's access to spring integration channels**...You don't believe it? Let's dive into it. What do we need?

Let's try to create simple Spring REST WS application running on Spring Boot with this URL template:

```
http://host:port/weather/{city}  

```

by hitting this url, we will return weather temperature in the entered {city}. How we are going to do that?

* First, **we're going to need to build the Spring Security infrastructure securing mentioned URL** with mask: /weather/**. Only users with roles USER and ADMIN are going to be allowed to access it.


```
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.authorizeRequests()
            	.antMatchers(HttpMethod.GET, "/weather/**").hasAnyRole("USER", "ADMIN").and()
            .formLogin().and()
            .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).
            logoutSuccessUrl("/weather-forecast-logout");
    }
```

* Next, **let's expose Spring REST endpoint**, which will be sending value of {city} path variable into our Spring Integration infrastructure as message header and waiting for the response on the specified channel. You can use many predefined Spring Integration inbound http gateways as Java API, **but sorry Spring team, XML still looks much better here**:


```
   <int-http:inbound-gateway id="inboundTestGateway"
		supported-methods="GET"
		request-channel="requestChannel"
		reply-channel="replyChannel"
		mapped-response-headers="Return-Status, Return-Status-Msg, HTTP_RESPONSE_HEADERS"
		path="/weather/{city}"
		reply-timeout="50000">

		<int-http:header name="city" expression="#pathVariables.city"/>

	</int-http:inbound-gateway>
```

Mentined XML says that we're going to be sending value of path variable {city} from URL http://host:port/weather/{city} into the Spring Integration channel named "requestChannel" as message header. Response will be retrieved from replyChannel.

* Now we've got secured REST entry point of our application. As second point, **we need to secure mentioned Spring Integration requestChannel**. Let's say that only user with role USER (watch out, Spring Boot prepends authorized role with prefix ROLE_) will be allowed to send message into "requestChannel"...


```
    @Bean
	@SecuredChannel(interceptor = "channelSecurityInterceptor", 
					sendAccess = "ROLE_USER")
	public MessageChannel requestChannel() {
	    return new DirectChannel();
	}
	
	@Bean
	public MessageChannel replyChannel() {
	    return new DirectChannel();
	}
	
	@SuppressWarnings("unchecked")
	@Bean
	public AccessDecisionManager accessDecisionManager() {
	    RoleVoter roleVoter = new RoleVoter(); 
	    final AffirmativeBased acd = new AffirmativeBased(Arrays.<AccessDecisionVoter<? extends Object>> 
	        asList(roleVoter));
	    return acd;
	}
	
	@Bean
	public ChannelSecurityInterceptor channelSecurityInterceptor(AuthenticationManager authenticationManager,
	                AccessDecisionManager accessDecisionManager) {
	    ChannelSecurityInterceptor channelSecurityInterceptor = new ChannelSecurityInterceptor();
	    channelSecurityInterceptor.setAuthenticationManager(authenticationManager);
	    channelSecurityInterceptor.setAccessDecisionManager(accessDecisionManager);
	    return channelSecurityInterceptor;
	}
```
Yes, you need to define accessDecisionManager, because Spring Boot doesn't expose it as Spring Bean and of course channelSecurityInterceptor, which is fully benefiting from our previously created Spring Security infrastructure.
### Howto test it ###

Perform...

* mvn clean install
* java -jar <path to compile jar>

from your browser, hit following URL, for example:

http://localhost:8080/weather/Prague

log in as **user/user** and you should receive: "Weather in the city Prague is 34.

now logout by hitting the URL http://localhost:8080/logout

and enter new new URL, for example: http://localhost:8080/weather/Madrid

login as **admin/admin** and you should receive 403, access denied. Because Spring Integration Security denied you send access to requestChannel. Of course you can experiment with that, try to change role in the @SecuredChannel annotation to ROLE_ADMIN for example.

Congratulations, you have created Spring Rest application with two levels of security, based on Spring Integration and Spring Security!