package demo.rest.controllers.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogoutPage {

	@RequestMapping("/weather-forecast-logout")
	public String displayLogoutMessage() {
		return "You have been logged out!";
	}
}
