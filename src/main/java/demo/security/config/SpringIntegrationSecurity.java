package demo.security.config;


import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.security.channel.ChannelSecurityInterceptor;
import org.springframework.integration.security.channel.SecuredChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.authentication.AuthenticationManager;

@Configuration
@EnableIntegration
@ImportResource({"classpath:inboundHttpGatewayConfig.xml"})
public class SpringIntegrationSecurity {
	
	@Bean
	@SecuredChannel(interceptor = "channelSecurityInterceptor", 
					sendAccess = "ROLE_USER")
	public MessageChannel requestChannel() {
	    return new DirectChannel();
	}
	
	@Bean
	public MessageChannel replyChannel() {
	    return new DirectChannel();
	}
	
	@SuppressWarnings("unchecked")
	@Bean
	public AccessDecisionManager accessDecisionManager() {
	    RoleVoter roleVoter = new RoleVoter(); 
	    final AffirmativeBased acd = new AffirmativeBased(Arrays.<AccessDecisionVoter<? extends Object>> 
	        asList(roleVoter));
	    return acd;
	}
	
	@Bean
	public ChannelSecurityInterceptor channelSecurityInterceptor(AuthenticationManager authenticationManager,
	                AccessDecisionManager accessDecisionManager) {
	    ChannelSecurityInterceptor channelSecurityInterceptor = new ChannelSecurityInterceptor();
	    channelSecurityInterceptor.setAuthenticationManager(authenticationManager);
	    channelSecurityInterceptor.setAccessDecisionManager(accessDecisionManager);
	    return channelSecurityInterceptor;
	}
}