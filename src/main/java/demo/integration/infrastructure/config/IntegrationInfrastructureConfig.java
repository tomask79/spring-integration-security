package demo.integration.infrastructure.config;

import org.springframework.stereotype.Component;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

@Component
public class IntegrationInfrastructureConfig { 
	
	@ServiceActivator(inputChannel = "requestChannel", 
			outputChannel = "replyChannel",
			requiresReply="true"
			) 
	public String getWeatherData(final Message<?> cityMessage) {
		final String city = (String) cityMessage.getHeaders().get("city");
		System.out.println("Received input "+city);
		return "Weather in the city "+city+" is "+34;
	}
}
